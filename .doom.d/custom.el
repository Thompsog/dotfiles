(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files
   '("/home/glenn/org/Tasks.org" "/home/glenn/org/AnimeList.org" "/home/glenn/org/Archive.org" "/home/glenn/org/Habits.org" "/home/glenn/org/Journal.org" "/home/glenn/org/Mail.org" "/home/glenn/org/Metrics.org" "/home/glenn/org/OPSReport.org" "/home/glenn/org/README.org" "/home/glenn/org/To-Do.org" "/home/glenn/org/Work - Someday.org" "/home/glenn/org/Work Schedule.org" "/home/glenn/org/Yemen.org" "/home/glenn/org/approved_leave.org" "/home/glenn/org/leave.org" "/home/glenn/org/my-life.org" "/home/glenn/org/new-org.org" "/home/glenn/org/org-theme-chooser.org" "/home/glenn/org/phone.org" "/home/glenn/org/readtheorg.org" "/home/glenn/org/todo.org"))
 '(package-selected-packages
   '(nov emacsql-sqlite3 deft sqlite3 org-present org-beautify-theme org-link-beautify org-tag-beautify org-bullets vterm org-mime org-superstar org-super-agenda org-journal mu4e-views)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(font-lock-comment-face ((t (:slant italic))))
 '(font-lock-keyword-face ((t (:slant italic)))))
(put 'customize-themes 'disabled nil)
